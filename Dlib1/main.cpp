#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <numeric>

using namespace dlib;
using namespace std;

// ----------------------------------------------------------------------------------------

int main(int argc, char** argv)
{
	try
	{
		if (argc == 1)
		{
			std::cout << "Specify image name as a command line argument" << endl;
			return 0;
		}

		// We need a face detector.  We will use this to get bounding boxes for
		// each face in an image.
		frontal_face_detector detector = get_frontal_face_detector();

		shape_predictor sp;
		deserialize("shape_predictor_68_face_landmarks.dat") >> sp;

		image_window win, win_aligned_faces;
		// Loop over all the images provided on the command line.
		for (int i = 1; i < argc; ++i)
		{
			std::cout << "processing image " << argv[i] << endl;
			array2d<rgb_pixel> img;
			load_image(img, argv[i]);

			// Make the image larger so we can detect small faces.
			//pyramid_up(img);

			// Now tell the face detector to give us a list of bounding boxes
			// around all the faces in the image.
			std::vector<rectangle> dets = detector(img);

			std::cout << "Number of faces detected: " << dets.size() << endl;

			// Now we will go ask the shape_predictor to tell us the pose of
			// each face we detected.
			std::vector<full_object_detection> shapes;
			for (unsigned long j = 0; j < dets.size(); ++j)
			{
				full_object_detection shape = sp(img, dets[j]);
				std::cout << "number of parts: " << shape.num_parts() << endl;
				shapes.push_back(shape);
			}

			// Now let's view our face poses on the screen.
			win.clear_overlay();
			win.set_image(img);
			win.add_overlay(render_face_detections(shapes));

			// We can also extract copies of each face that are cropped, rotated upright,
			// and scaled to a specified size and padding as shown here:
			int face_image_size = 400;
			float padding = 0.4;
			dlib::array<array2d<rgb_pixel> > face_chips;
			extract_image_chips(img, get_face_chip_details(shapes, face_image_size, padding), face_chips);

			// Create image with aligned face on it
			// for further processing
			// In our case we suppose that there's only
			// one face on the image
			array2d<rgb_pixel> aligned_face_img;
			assign_image(aligned_face_img, face_chips[0]);

			// Now we detect face position after alignment
			std::vector<rectangle> dets_aligned = detector(aligned_face_img);

			// Find landmarks positions on the aligned face image
			// Then calculate output information and write it
			// to file
			std::vector<full_object_detection> shapes_aligned;
			full_object_detection shape = sp(aligned_face_img, dets_aligned[0]);
			shapes_aligned.push_back(shape); 

			// Calculate interocular distance as a horizontal distance
			// between landmarks number 40 and 43
			double interocular_dist = sqrt(pow((shape.part(39).x() - shape.part(42).x()), 2) +
				pow((shape.part(39).y() - shape.part(42).y()), 2));

			// Nose bridge will be the reference point
			int nose_bridge_x = shape.part(27).x();
			int nose_bridge_y = shape.part(27).y();

			std::cout << nose_bridge_x << ' ' << nose_bridge_y << endl;

			// Calculate vertical distance between lower
			// and upper eyelids
			double lower_eyelid_left, upper_eyelid_left, lower_eyelid_right, upper_eyelid_right;
			double eyelids_dist_left, eyelids_dist_right;
			lower_eyelid_left = (shape.part(46).y() + shape.part(47).y()) / 2;
			upper_eyelid_left = (shape.part(43).y() + shape.part(44).y()) / 2;
			eyelids_dist_left = (lower_eyelid_left - upper_eyelid_left) / interocular_dist;
			lower_eyelid_right = (shape.part(40).y() + shape.part(41).y()) / 2;
			upper_eyelid_right = (shape.part(37).y() + shape.part(38).y()) / 2;
			eyelids_dist_right = (lower_eyelid_right - upper_eyelid_right) / interocular_dist;

			// Calculate mouth width and height
			std::vector<int> upper_lip;
			upper_lip.push_back(shape.part(50).y());
			upper_lip.push_back(shape.part(51).y());
			upper_lip.push_back(shape.part(52).y());
			upper_lip.push_back(shape.part(61).y());
			upper_lip.push_back(shape.part(62).y());
			upper_lip.push_back(shape.part(63).y());
			std::vector<int> lower_lip;
			lower_lip.push_back(shape.part(56).y());
			lower_lip.push_back(shape.part(57).y());
			lower_lip.push_back(shape.part(58).y());
			lower_lip.push_back(shape.part(65).y());
			lower_lip.push_back(shape.part(66).y());
			lower_lip.push_back(shape.part(67).y());
			// Calculate average y coordinates for lower and upper lips
			// then calculate mouth height
			double mouth_height = (accumulate(lower_lip.begin(), lower_lip.end(), 0.0)/ lower_lip.size() -
				accumulate(upper_lip.begin(), upper_lip.end(), 0.0) / upper_lip.size()) / interocular_dist;
			int left_mouth_corner, right_mouth_corner;
			left_mouth_corner = shape.part(54).x();
			right_mouth_corner = shape.part(48).x();
			double mouth_width = (left_mouth_corner - right_mouth_corner) / interocular_dist;

			// Calculate nostrils width
			double nostrils_width = (shape.part(35).x() - shape.part(31).x()) / interocular_dist;

			// Output file
			ofstream fout;
			fout.open("output.txt");
			fout << interocular_dist << ";\n";
			fout << eyelids_dist_left << " ";
			fout << eyelids_dist_right << ";\n";
			fout << mouth_height << " ";
			fout << mouth_width << ";\n";
			fout << nostrils_width << ";\n";
			
			// Write all landmarks coordinates relative to
			// the reference point (nose bridge)
			for (unsigned int j = 0; j < shape.num_parts(); ++j)
			{
				fout << shape.part(j).x() - nose_bridge_x << ' ';
				fout << shape.part(j).y() - nose_bridge_y << ";\n";
			}
			fout.close();

			// save_png(aligned_face_img, "out.png");
			win_aligned_faces.clear_overlay();
			win_aligned_faces.set_image(aligned_face_img);
			win_aligned_faces.add_overlay(render_face_detections(shapes_aligned));

			std::cout << "Hit enter to process the next image..." << endl;
			std::cin.get();
		}
		std::cout << "The end" << endl;
		return 0;
	}
	catch (exception& e)
	{
		std::cout << "\nexception thrown!" << endl;
		std::cout << e.what() << endl;
	}
}